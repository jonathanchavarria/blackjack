const suits = ["♠", "♡", "♢", "♣"];
const ranks = ["Ace", 2, 3, 4, 5, 6, 7, 8, 9, 10, "Knave", "Queen", "King"];

function Card(suit, rank) {
    this.suit = suit;
    this.rank = rank;
  }


function getDeck(suits, ranks){
    let deck = [];
    for (const suit of suits) {
        for (const rank of ranks){
                deck.push(new Card(suit, rank));
        }
    }
    console.log(deck);
    return deck;
}

function getRandomCard(deck){
    let index = Math.floor(Math.random() * (52 - 0) + 0);
    console.log(index);
    console.log(deck[index]);
    console.log(`Your card is ${deck[index].rank} of ${deck[index].suit}. Did you bust?`);
    return deck[index];
}

function dealToDisplay(card) {
    const newCardElement = document.createElement("li");
    newCardElement.setAttribute("data-blackjack-value", rankToValue(card.value))
    newCardElement.innerText = (`${rankToWord(card.value)} of ${suitToWord(card.suit)}`)

    const PlayersCards = document.querySelector("ol#players_cards_list")
    PlayersCards.appendChild(newCardElement)
}

function rankToWord(rank) {
    const mapRanksToWords = { '2': "Two", '3': "Three", '4': "Four", '5': "Five", '6': "Six", '7': "Seven", '8': "Eight", '9': "Nine", '10': "Ten", 'JACK': 'Jack', 'QUEEN' : 'Queen', 'KING': 'King', 'ACE' : 'Ace'  };
    if (mapRanksToWords[rank] === undefined) {
        return rank
    } else {
        return mapRanksToWords[rank]
    }
}

function suitToWord(suit) {
    const mapSuitsToWord = {"SPADES":"Spades", "HEARTS":"Hearts", "DIAMONDS":"Diamonds", "CLUBS": "Clubs", "":"Mystery",}
    return mapSuitsToWord[suit]
}


function rankToValue(rank, down=false) {
    const mapRankToValue = { "QUEEN": "10", "KING": "10", "JACK": "10", "ACE": "11/1", "Face Down": "?", }
    if (down === true){
        return mapRankToValue["Face Down"]
    }
     else if (mapRankToValue[rank] === undefined) {
        return rank.toString()
    } else{
        return mapRankToValue[rank]
    }
}

function dealRandomCard(deck) {
    dealToDisplay(getRandomCard(deck));
  }



function getBankroll(){
    return bankroll;
}

function setBankroll(newBalance){
    bankroll = newBalance;
    localStorage.setItem("bankroll", String(newBalance))
}


function timeToBet(){
    const playersActions = document.querySelector("#playersActions");
    playersActions.classList.add("displayNone");
    bankrollSpan.textContent = `$${bankroll} `;
    bettingInterface.classList.remove("displayNone");


}

function makeWager(wagerInput){
    console.log(wagerInput.value);
    timeToPlay();
    
}

function timeToPlay(){
    const bettingInterface = document.querySelector("#betting");
    bettingInterface.classList.add("displayNone");
    const playersActions = document.querySelector("#playersActions");
    playersActions.classList.remove("displayNone");
    var down_card = drawFourCards(dealFourCards);
    console.log(down_card);

    countdown = setTimeout(() => { dealersTurn(); console.log("player timeout");}, "10000");

}






const hit = document.querySelector('#hit-button');

let deck = getDeck(suits, ranks);
hit.addEventListener("click", function(){drawOneCard(dealOneCard)});
hit.addEventListener("click", function(){clearInterval(countdown)});


let bankroll = JSON.parse(localStorage.getItem("bankroll")) || 2022;

const things = document.querySelector("#things");
const bettingInterface = document.createElement("section");
bettingInterface.setAttribute("id", "betting");
bettingInterface.setAttribute("class", "mdc-layout-grid__cell--span-6 mdc-layout-grid__cell--span-8-tablet mdc-card space-between displayNone");
const bankrollSpan = document.createElement("span");
const wagerLabel= document.createElement("label");
wagerLabel.setAttribute("for", "users-wager");
wagerLabel.innerHTML = "Wager Amount:";
bankrollSpan.textContent = `Bankroll: $${bankroll} `;
bettingInterface.appendChild(bankrollSpan);
const wagerInput = document.createElement("input");
wagerInput.setAttribute("type", "number");
wagerInput.setAttribute("class", "mdc-text-field__input");
wagerInput.setAttribute("id", "#users-wager");
wagerLabel.appendChild(wagerInput);
bettingInterface.appendChild(wagerLabel);
const betButton = document.createElement("button");
betButton.setAttribute("class", "mdc-button");
betButton.textContent = "Wager";
betButton.addEventListener("click", function() {makeWager(wagerInput);});
bettingInterface.appendChild(betButton);
things.appendChild(bettingInterface);


function getShoe(callback){
    fetch('https://www.deckofcardsapi.com/api/deck/new/shuffle?deck_count=6')
    .then((response) => response.json())
    .then((data) => {callback(data)})
        
}


function drawFourCards(callback){
     fetch(`https://www.deckofcardsapi.com/api/deck/${deckId}/draw?count=4`)
     .then((response) => response.json())
     .then((data) => {callback(data.cards)})
 }

function drawOneCard(callback){
    fetch(`https://www.deckofcardsapi.com/api/deck/${deckId}/draw?count=1`)
    .then((response) => response.json())
    .then((data) => {callback(data.cards)})
}

function dealOneCard(cards){
    dealToPlayer(cards[0]);
    getPlayersScore();
    //getDealersScore();
}

function dealerHit(cards){
    dealToDealer(cards[0]);
    //getDealersScore();
}

var deckId = "None";
getShoe(function (deckdata) {
    deckId = deckdata.deck_id;
});



function dealFourCards(cards){
    dealToPlayer(cards[0]);
    dealToPlayer(cards[2]);
    dealToDealer(cards[1], true);
    dealToDealer(cards[3], false);
    getPlayersScore();
    getDealersScore();
    console.log(`DOWN CARD VALUE: ${rankToValue(cards[1].value)} `);
    down_card_value = rankToValue(cards[1].value);
}

function dealToPlayer(card){
    const newCardElement = document.createElement("li");
    newCardElement.setAttribute("data-blackjack-value", rankToValue(card.value))
    //newCardElement.innerText = (`${rankToWord(card.value)} of ${suitToWord(card.suit)}`)
    newCardElement.innerHTML = `<img src=${card.image}>`;
    const PlayersCardsElement = document.querySelector("ol#players_cards_list")
    PlayersCardsElement.appendChild(newCardElement)
}

function dealToDealer(card, down){
    const newCardElement = document.createElement("li");
    if (down === true){
        newCardElement.setAttribute("class", "downcard");
    }
    newCardElement.setAttribute("data-blackjack-value", rankToValue(card.value, down))
    //newCardElement.innerText = (`${rankToWord(card.value)} of ${suitToWord(card.suit)}`)
    if (down === true){
        //newCardElement.innerHTML = `<img src=https://deckofcardsapi.com/static/img/back.png>`;
        newCardElement.innerHTML = `<img src=https://deckofcardsapi.com/static/img/back.png>`;
    } else {
        newCardElement.innerHTML = `<img src=${card.image}>`;
    }
    
    const DealersCards = document.querySelector("ol#dealers_cards_list")
    DealersCards.appendChild(newCardElement)
}

var down_card_value = 0; 
const stand = document.querySelector('#stand-button');
stand.addEventListener("click", function(){clearInterval(countdown)});

stand.addEventListener("click", function(){dealersTurn()});

timeToBet();

async function dealersTurn(){
    var dealer_current_score = getDealersScore(parseInt(down_card_value));
    while (dealer_current_score < 17 && dealer_current_score != -1) {
       
        drawOneCard(dealerHit);  
        await sleep(400);
        dealer_current_score = getDealersScore(parseInt(down_card_value));
    }
    if (dealer_current_score > getPlayersScore()){
        console.log("the house always wins")
        endGame(false);
    } else{
        console.log("the house will always win.")
        endGame(true);
    }
  
}

function endGame(win){
    if (win === true){
        bankroll = parseInt(bankroll) + wagerInput.value;

    } else {
        bankroll = parseInt(bankroll) - wagerInput.value;
    }
    bankrollSpan.textContent = (`${bankroll}`);
    clearBoard();
    timeToBet();
}

function clearBoard(){
    const PlayersCardsElement = document.querySelector("ol#players_cards_list")
    while (PlayersCardsElement.firstChild){
        PlayersCardsElement.removeChild(PlayersCardsElement.firstChild);
    }
    const DealersCardsElement = document.querySelector("ol#dealers_cards_list")
    while (DealersCardsElement.firstChild){
        DealersCardsElement.removeChild(DealersCardsElement.firstChild);
    }
}

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

function getPlayersScore(){
    const PlayersCardsElement = document.querySelector("ol#players_cards_list")
    const cards = PlayersCardsElement.getElementsByTagName("li");
    var score = 0
    var score_values = []
    for (card of cards){
        number = parseInt(card.getAttribute("data-blackjack-value"));
        score_values.push(number);
        
    }
    score = score_values.reduce((a, b) => a + b, 0);

    var eleven_index = score_values.indexOf(11);
    while (score > 21 && eleven_index != -1){
        score_values[eleven_index] = 1;
        eleven_index = score_values.indexOf(11);
    }
    score = score_values.reduce((a, b) => a + b, 0);
    const PlayerScoreSpan = document.querySelector("#players_score");
    console.log(`Players Score: ${score}`);
    PlayerScoreSpan.textContent = `${score}`;
    if (score > 21){
        console.log(`player busted`);
        PlayerScoreSpan.textContent = `BUSTED!!`;
        return -1;
    }
    return score;
}

function getDealersScore(down_value = 0){
    const DealersCardsElement = document.querySelector("ol#dealers_cards_list")
    const cards = DealersCardsElement.getElementsByTagName("li");
    var score = 0
    var score_values = []
    for (card of cards){
        number = parseInt(card.getAttribute("data-blackjack-value"));
        if (isNaN(number)){
            score_values.push(0);
        } else if (number === 1){
            score_values.push(11);
        } else {
            score_values.push(number);
        }
        
    }
    score_values.push(down_value);
    score = score_values.reduce((a, b) => a + b, 0);

    var eleven_index = score_values.indexOf(11);
    while (score > 21 && eleven_index != -1){
        score_values[eleven_index] = 1;
        eleven_index = score_values.indexOf(11);
    }
    score = score_values.reduce((a, b) => a + b, 0);
    //score += down_value;
    const DealerScoreSpan = document.querySelector("#dealers_score");
    console.log(`Dealer Score: ${score}`);
    DealerScoreSpan.textContent = ` ${score} `;
    if (score > 21){
        console.log(`dealer busted`);
        DealerScoreSpan.textContent = `BUSTED!!`;
        return -1;
    }
    return score;
}
var countdown;