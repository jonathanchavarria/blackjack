const signUpForm = document.forms[0];
const signUpButton = signUpForm[8];


function isNotEmpty(input) {
    return input.value !== "";
}

function noEmptyFields() {
    return signUpFormInputs.every(isNotEmpty);
}
  
function passwordsMatch() {
    return passwordInput.value === confirmInput.value;
}
  
function legalCheckboxChecked() {
    return signUpForm[6].checked;
}

function termsCheckboxChecked() {
    return signUpForm[7].checked;
}

function printInputs(input){
    return console.log(`${input.id}: ${input.value}`)
}

function ageCheck(){
    return signUpForm[1].value > 12;
}
  
function validateEligibility(event) {
    event.preventDefault();

    signUpFormInputs.forEach(element => console.log(`${element.id}: ${element.value}`))
    if (termsCheckboxChecked()){
        console.log("The user has checked the terms checkbox")
    } else {
        console.log("The user has not checked the terms checkbox")
    }

    if (legalCheckboxChecked()){
        console.log("The user has checked the legal checkbox")
    } else {
        console.log("The user has not checked the legal checkbox")
    }

    if (noEmptyFields() && passwordsMatch() && legalCheckboxChecked() && termsCheckboxChecked() && ageCheck()) {
      console.log("The user is eligible");
    } else {
      console.log("The user is ineligible");
    }
}

const birthdateInput = signUpForm[0];
const ageInput = signUpForm[1];
const fullnameInput = signUpForm[2];
const usernameInput = signUpForm[3];
const passwordInput = signUpForm[4];
const confirmInput = signUpForm[5];
const legalCheckbox = signUpForm[6];
const termsCheckbox = signUpForm[7];

const signUpFormInputs = [
    birthdateInput,
    ageInput,
    fullnameInput,
    usernameInput,
    passwordInput,
    confirmInput,
];

signUpButton.addEventListener("click", validateEligibility);
