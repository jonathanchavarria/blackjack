
const MDCTopAppBar = mdc.topAppBar.MDCTopAppBar;
const topAppBarElement = document.querySelector('.mdc-top-app-bar');
const topAppBar = new MDCTopAppBar(topAppBarElement);

const MDCCheckbox = mdc.checkbox.MDCCheckbox;
const checkboxes = [].map.call(
  document.querySelectorAll(".mdc-checkbox"),
  function (el) {
    return new MDCCheckbox(el);
  }
);


const MDCTextField = mdc.textField.MDCTextField;
const foos = [].map.call(
  document.querySelectorAll(".mdc-text-field"),
  function (el) {
    return new MDCTextField(el);
  }
);
